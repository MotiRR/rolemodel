package edu.vtb;

import edu.vtb.modules.Authority;
import edu.vtb.modules.Role;
import edu.vtb.modules.RoleOfAuthority;
import edu.vtb.modules.User;
import edu.vtb.repository.AuthorityDAO;
import edu.vtb.repository.RoleDAO;
import edu.vtb.repository.RoleOfAuthorityDAO;
import edu.vtb.repository.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;
import java.util.List;


@SpringBootApplication
public class Main implements CommandLineRunner {

    @Autowired
    UserDAO userDAO;
    @Autowired
    AuthorityDAO authorityDAO;
    @Autowired
    RoleDAO roleDAO;
    @Autowired
    RoleOfAuthorityDAO roleOfAuthorityDAO;


    @Override
    public void run(String[] args) throws Exception {
        User user_1, user_2;
        Role role_1, role_2, role_3;
        Authority authority_1, authority_2, authority_3, authority_4;
        RoleOfAuthority roleOfAuthority_1, roleOfAuthority_2, roleOfAuthority_3, roleOfAuthority_4;

        authority_1 = new Authority();
        authority_1.setName("Анализ по всем финансам");

        authority_2 = new Authority();
        authority_2.setName("Консультация");

        authority_3 = new Authority();
        authority_3.setName("Планирование");

        authority_4 = new Authority();
        authority_4.setName("Анализ расходов");

        role_1 = new Role();
        role_1.setName("Финансовый аналитик");
        role_2 = new Role();
        role_2.setName("Финансовый консультант");
        role_3 = new Role();
        role_3.setName("Бухгалтер");

        roleOfAuthority_1 = new RoleOfAuthority();
        roleOfAuthority_1.setAuthority(authority_1);
        roleOfAuthority_1.setRole(role_1);

        roleOfAuthority_2 = new RoleOfAuthority();
        roleOfAuthority_2.setAuthority(authority_3);
        roleOfAuthority_2.setRole(role_1);

        roleOfAuthority_3 = new RoleOfAuthority();
        roleOfAuthority_3.setAuthority(authority_3);
        roleOfAuthority_3.setRole(role_2);

        roleOfAuthority_4 = new RoleOfAuthority();
        roleOfAuthority_4.setAuthority(authority_4);
        roleOfAuthority_4.setRole(role_3);

        authority_1.setRoleOfAuthorities(Arrays.asList(roleOfAuthority_1));
        authorityDAO.save(authority_1);

        authority_3.setRoleOfAuthorities(Arrays.asList(roleOfAuthority_2, roleOfAuthority_3));
        authorityDAO.save(authority_3);

        authority_4.setRoleOfAuthorities(Arrays.asList(roleOfAuthority_4));
        authorityDAO.save(authority_4);

        user_1 = new User();
        user_1.setName("Иван");
        role_1.setUser(user_1);
        role_2.setUser(user_1);
        user_1.setRoles(Arrays.asList(role_1, role_2));
        userDAO.save(user_1);

        user_2 = new User();
        user_2.setName("Петя");
        role_3.setUser(user_2);
        user_2.setRoles(Arrays.asList(role_3));
        userDAO.save(user_2);

        roleOfAuthorityDAO.save(roleOfAuthority_1);
        roleOfAuthorityDAO.save(roleOfAuthority_2);
        roleOfAuthorityDAO.save(roleOfAuthority_3);
        roleOfAuthorityDAO.save(roleOfAuthority_4);

        int id_user = 4, id_authority = 1;
        System.out.format("Есть ли у пользователя с id = %d, полномочие с " +
                "id = %d " + userDAO.haveAuthority(id_user, id_authority) + "\n", id_user, id_authority);
        System.out.format("Список полномочий пользователя с id = %d\n", id_user);
        List list = userDAO.putAllAuthority(id_user);
        if (list.isEmpty()) System.out.format("Полномочий нет");
        else
            list.forEach(System.out::println);

    }

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Main.class, args);
    }
}
