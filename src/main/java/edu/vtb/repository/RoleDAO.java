package edu.vtb.repository;

import edu.vtb.modules.Role;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional
public class RoleDAO {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(Role role) {
        em.persist(role);
    }
}
