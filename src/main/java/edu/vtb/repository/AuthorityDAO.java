package edu.vtb.repository;

import edu.vtb.modules.Authority;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional
public class AuthorityDAO {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(Authority authority) {
        em.persist(authority);
    }

}
