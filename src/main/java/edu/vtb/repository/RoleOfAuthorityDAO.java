package edu.vtb.repository;

import edu.vtb.modules.RoleOfAuthority;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional
public class RoleOfAuthorityDAO {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(RoleOfAuthority roleOfAuthority) {
        em.persist(roleOfAuthority);
    }
}
