package edu.vtb.modules;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Authority {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    @OneToMany(mappedBy = "authority")
    private List<RoleOfAuthority> roleOfAuthorities;
}
