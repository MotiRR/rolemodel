package edu.vtb.modules;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
@Data
public class RoleOfAuthority {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "authority_id")
    private Authority authority;
    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;
}
