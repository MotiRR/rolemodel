package edu.vtb.modules;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Role {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    @ManyToOne
    private User user;
    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL)
    private List<RoleOfAuthority> roleOfAuthorities;
}
